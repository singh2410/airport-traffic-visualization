#!/usr/bin/env python
# coding: utf-8

# # Airport Traffic Visualization
# #By- Aarush Kumar
# #Dated: July 12,2021

# In[1]:


get_ipython().system('pip install geoplot')


# In[2]:


from datetime import date
import os
import geopandas as gpd
import geoplot as gplt
import folium
import mapclassify
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import plotly.express as px
import re
import seaborn as sns
from shapely.geometry import Point, Polygon
from shapely.geometry import MultiPolygon


# In[3]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[4]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Airport Traffic Visualization/covid_impact_on_airport_traffic.csv')


# In[5]:


df


# ## EDA

# In[6]:


df.shape


# In[7]:


df.size


# In[8]:


df.info()


# In[9]:


df.isnull().sum()


# In[10]:


df.dtypes


# In[11]:


df.describe()


# In[12]:


df["Date"] =df["Date"].map(lambda x: date.fromisoformat(x))


# In[13]:


df["weekday"] = df["Date"].map(lambda x: x.weekday())
w_list = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
df["weekday"] = [w_list[idx] for idx in df["weekday"]]


# In[14]:


def cut_long(point):
    #point is like 'POINT(151.180087713813 -33.9459774986125)'
    long, _ = point[6:-1].split(" ")
    return float(long)
def cut_lat(point):
    #point is like 'POINT(151.180087713813 -33.9459774986125)'
    _, lat = point[6:-1].split(" ")
    return float(lat)


# In[15]:


df["long"] = df["Centroid"].map(cut_long)
df["lat"] = df["Centroid"].map(cut_lat)


# In[16]:


need_columns = [col for col in df.columns if not col in ["AggregationMethod", "Version", "Centroid"]]
df = df[need_columns]


# In[17]:


df.head()


# In[18]:


covid_impact_on_airport_traffic_unique = df[~df[["AirportName"]].duplicated()].reset_index(drop=True)
covid_impact_on_airport_traffic_unique


# In[19]:


df_geometry = covid_impact_on_airport_traffic_unique[['AirportName', 'City', 'State',
                                                     'ISO_3166_2', 'Country', 'long', 'lat']]


# In[20]:


def visualize_airport_map(df,  zoom):
    
    lat_map=30.038557
    lon_map=31.231781
    f = folium.Figure(width=1000, height=500)
    m = folium.Map([lat_map,lon_map], zoom_start=zoom).add_to(f)
        
    for i in range(0,len(df)):
        folium.Marker(location=[df["lat"][i],df["long"][i]],icon=folium.Icon(icon_color='white',icon ='plane',prefix='fa')).add_to(m)
        
    return m


# In[21]:


visualize_airport_map(covid_impact_on_airport_traffic_unique, 1)


# In[22]:


df_Country_count = pd.DataFrame(df["Country"].value_counts())
#df_Country_count
g = df_Country_count.plot.pie(y='Country', figsize=(7, 7))
g.set_title("records for each country")


# In[23]:


plt.figure(figsize=(10, 5))
g = sns.countplot(data=df, x="AirportName",
              order = df['AirportName'].value_counts().index)
g.set_xticklabels(g.get_xticklabels(), rotation=90)
g.set_title("records for each airport")


# In[24]:


df_month_count = pd.DataFrame(df["Date"].map(lambda d: d.month).value_counts())
df_month_count = df_month_count.reset_index()
df_month_count = df_month_count.rename(columns={"Date":"count", "index":"month"})
g = sns.barplot(data=df_month_count.reset_index(), y="count", x="month")
g.set_xticklabels(g.get_xticklabels(), rotation=90)
g.set_title("records for each month")


# In[25]:


df_weekday_count = pd.DataFrame(df["weekday"].value_counts())
g = df_weekday_count.plot.pie(y='weekday', figsize=(7, 7))
g.set_title("records for each weekday")


# In[26]:


gdf = gpd.GeoDataFrame(covid_impact_on_airport_traffic_unique, 
                       geometry=gpd.points_from_xy(covid_impact_on_airport_traffic_unique.long, covid_impact_on_airport_traffic_unique.lat))


# In[27]:


gdf.head()


# In[28]:


world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
world.head()


# In[29]:


g = world.plot(color='white', edgecolor='gray')
g.set_title("example world map")


# In[30]:


ax = world.plot(color='white', edgecolor='gray', figsize=(15, 10))
g = gdf.plot(ax=ax, marker='*', color='red', markersize=50)
g.set_title("example world map with marker")
plt.show()


# In[31]:


fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
world.plot(ax=ax, color='white', edgecolor='gray')
g = gdf.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
g.set_title("example world map with ")
plt.show()


# In[32]:


df_airport_weekday_mean = df[["AirportName", "weekday", "PercentOfBaseline"]].groupby(["AirportName", "weekday"]).mean()
df_airport_weekday_mean = df_airport_weekday_mean.reset_index()
df_airport_weekday_mean = df_airport_weekday_mean.merge(df_geometry, on="AirportName")


# In[33]:


df_airport_weekday_mean.head()


# In[34]:


df_airport_weekday_mean_sun = df_airport_weekday_mean[df_airport_weekday_mean["weekday"]=="Sun"]
gdf_airport_weekday_mean_sun = gpd.GeoDataFrame(df_airport_weekday_mean_sun, 
                                                geometry=gpd.points_from_xy(df_airport_weekday_mean_sun.long, df_airport_weekday_mean_sun.lat))


# In[35]:


fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
world.plot(ax=ax, color='white', edgecolor='gray')
gdf_airport_weekday_mean_sun.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
plt.show()


# In[36]:


df_airport_weekday_mean_wed = df_airport_weekday_mean[df_airport_weekday_mean["weekday"]=="Wed"]
gdf_airport_weekday_mean_wed = gpd.GeoDataFrame(df_airport_weekday_mean_wed, 
                                                geometry=gpd.points_from_xy(df_airport_weekday_mean_wed.long, df_airport_weekday_mean_wed.lat))
fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
world.plot(ax=ax, color='white', edgecolor='gray')
gdf_airport_weekday_mean_wed.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
plt.show()


# In[37]:


df_airport_date_mean = df[["AirportName", "Date", "PercentOfBaseline"]]
df_airport_date_mean["Date"] = df_airport_date_mean["Date"].map(lambda d: d.month)
df_airport_date_mean = df_airport_date_mean.groupby(["AirportName", "Date"]).mean()
df_airport_date_mean = df_airport_date_mean.reset_index()
df_airport_date_mean = df_airport_date_mean.merge(df_geometry, on="AirportName")
df_airport_date_mean = df_airport_date_mean.rename(columns={"Date": "month"})


# In[38]:


df_airport_date_mean_3 = df_airport_date_mean[df_airport_date_mean["month"]==3]
df_airport_date_mean_3 = gpd.GeoDataFrame(df_airport_date_mean_3, 
                                                geometry=gpd.points_from_xy(df_airport_date_mean_3.long, df_airport_date_mean_3.lat))
fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
world.plot(ax=ax, color='white', edgecolor='gray')
g = df_airport_date_mean_3.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
g.set_title("PercentOfBaseline of May")
plt.show()


# In[39]:


df_airport_date_mean_4 = df_airport_date_mean[df_airport_date_mean["month"]==4]
df_airport_date_mean_4 = gpd.GeoDataFrame(df_airport_date_mean_4, 
                                                geometry=gpd.points_from_xy(df_airport_date_mean_4.long, df_airport_date_mean_4.lat))
fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)
world.plot(ax=ax, color='white', edgecolor='gray')
g = df_airport_date_mean_4.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
g.set_title("PercentOfBaseline of April")
plt.show()


# In[40]:


df_airport_date_mean_5 = df_airport_date_mean[df_airport_date_mean["month"]==5]
df_airport_date_mean_5 = gpd.GeoDataFrame(df_airport_date_mean_5, 
                                                geometry=gpd.points_from_xy(df_airport_date_mean_5.long, df_airport_date_mean_5.lat))

fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)

world.plot(ax=ax, color='white', edgecolor='gray')
g = df_airport_date_mean_5.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
g.set_title("PercentOfBaseline of May")
plt.show()


# In[41]:


df_airport_date_mean_6 = df_airport_date_mean[df_airport_date_mean["month"]==6]
df_airport_date_mean_6 = gpd.GeoDataFrame(df_airport_date_mean_6, 
                                                geometry=gpd.points_from_xy(df_airport_date_mean_6.long, df_airport_date_mean_6.lat))

fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)

world.plot(ax=ax, color='white', edgecolor='gray')
g = df_airport_date_mean_6.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
g.set_title("PercentOfBaseline of June")
plt.show()


# In[42]:


df_airport_date_mean_7 = df_airport_date_mean[df_airport_date_mean["month"]==7]
df_airport_date_mean_7 = gpd.GeoDataFrame(df_airport_date_mean_7, 
                                                geometry=gpd.points_from_xy(df_airport_date_mean_7.long, df_airport_date_mean_7.lat))

fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)

world.plot(ax=ax, color='white', edgecolor='gray')
g = df_airport_date_mean_7.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
g.set_title("PercentOfBaseline of July")
plt.show()


# In[43]:


df_airport_date_mean_8 = df_airport_date_mean[df_airport_date_mean["month"]==8]
df_airport_date_mean_8 = gpd.GeoDataFrame(df_airport_date_mean_8, 
                                                geometry=gpd.points_from_xy(df_airport_date_mean_8.long, df_airport_date_mean_8.lat))

fig, ax = plt.subplots(1, 1, figsize=(15, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.1)

world.plot(ax=ax, color='white', edgecolor='gray')
g = df_airport_date_mean_8.plot(column='PercentOfBaseline', ax=ax, markersize=50, legend=True, cax=cax)
g.set_title("PercentOfBaseline of August")
plt.show()


# In[44]:


df_airport_date_all_mean = df.groupby(["AirportName"]).mean()
df_airport_date_all_mean = df_airport_date_all_mean.reset_index()


# In[45]:


df_airport_date_all_mean.head()


# In[46]:


fig = px.scatter_mapbox(df_airport_date_all_mean,
                        lat="lat",
                        lon="long",
                        hover_name="AirportName",
                        hover_data=["PercentOfBaseline"],
                        color="PercentOfBaseline",
                        zoom=1,
                        height=600,
                        size="PercentOfBaseline",
                        size_max=30,
                        opacity=0.4,
                        width=1300)
fig.update_layout(mapbox_style='stamen-terrain')
fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
fig.update_layout(title_text="Mean of trafic on sunday")
fig.show()


# In[47]:


fig = px.scatter_mapbox(df_airport_date_all_mean,
                        lat="lat",
                        lon="long",
                        hover_name="AirportName",
                        hover_data=["PercentOfBaseline"],
                        color="PercentOfBaseline",
                        zoom=1,
                        height=600,
                        size="PercentOfBaseline",
                        size_max=30,
                        opacity=0.4,
                        width=1300)
fig.update_layout(mapbox_style='carto-positron')
fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
fig.update_layout(title_text="Mean of trafic on sunday")
fig.show()


# In[48]:


gdf = gdf[[col for col in gdf.columns if col not in ["geometry"]]]
gdf = gdf.rename(columns={'Geography': 'geometry'})


# In[49]:


def polygon_str2polygon(polygon_str):
    coodinates = re.split("[, ]", polygon_str[9:-2])
    coodinates = [float(item) for item in coodinates if item != ""]
    return Polygon([coodinates[i:i+2] for i in range(0,len(coodinates),2)])
    
gdf["geometry"] = gdf["geometry"].map(polygon_str2polygon)


# In[50]:


gdf_newyork = gdf[gdf["City"].isin(["New York"])]
gdf_newyork = gpd.GeoDataFrame(gdf_newyork)
gdf_newyork


# In[51]:


gdf_newyork.iloc[0]["geometry"]


# In[52]:


gdf_newyork.iloc[1]["geometry"]


# In[53]:


contiguous_usa = gpd.read_file(gplt.datasets.get_path('contiguous_usa'))
newyork_shape = contiguous_usa[contiguous_usa["state"].isin(["New York"])]
newyork_shape


# In[54]:


g = newyork_shape.plot(color='white', edgecolor='gray')
g.set_title("New York")


# In[55]:


gpd_per_person = gdf_newyork["PercentOfBaseline"]
scheme = mapclassify.Quantiles(gpd_per_person, k=5)

base1 = gplt.choropleth(
    gdf_newyork, #ax=base,
    hue=gpd_per_person, scheme=scheme,
    cmap='inferno_r', legend=True, figsize=(12, 12)
)

gplt.polyplot(
    newyork_shape, ax=base1,
    edgecolor='white',
    facecolor='lightgray',
    figsize=(12, 12)
)


# In[56]:


def create_point_geom(data):
    x = data[0]
    y = data[1]
    point = Point(x, y)
    return point


# In[57]:


gdf["geometry"] = gdf[["long", "lat"]].apply(create_point_geom, axis=1)


# In[58]:


ax = gplt.pointplot(
    gpd.GeoDataFrame(gdf),
    legend_kwargs={'orientation': 'horizontal'}, scale='PercentOfBaseline',limits=(5, 10),
    hue='PercentOfBaseline', legend=True, figsize=(15, 15)
)
gplt.polyplot(
    world, ax=ax, 
    edgecolor='None', facecolor='lightgray'
)

